#!/usr/bin/env python3
# -*- coding: utf-8 -*

import sys, os
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib, GObject
import RPi.GPIO as GPIO
import time
from datetime import timedelta
from colorama import init, Fore, Back, Style
import argparse

# Initializes Colorama
init(autoreset=True)

# Advanced parameters
files_location="Records" 
lights_pin = 5  # BCM pin 5, BOARD pin 29

try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-ct", "--capture_time",  help="defines the duration of the videos", type=int, default=60*10) # default 60*10 (10min)
    parser.add_argument("-cg", "--capture_gap",  help="defines the gap between the videos", type=int, default=0) # default 0
    parser.add_argument("-ci", "--capture_iterations",  help="defines the number of videos", type=int, default=6*12) # default 6*12 (9h->21h)
    parser.add_argument("-cc", "--capture_csi",  help="defines the cameras to use (0: Inference, 1: Hive)", type=int, default=[0,1], nargs="*") # default [0,1] (0:Inference, 1:Hive)
    parser.add_argument("-st", "--start_time",  help="defines a waiting time before starting the sequence of recordings", type=int, default=1) # default 1
    args = parser.parse_args()

    # Assert
    assert len(args.capture_csi) > 0, "Please add at least one camera to the csi_camera list."

    # Pin Setup
    GPIO.setmode(GPIO.BCM)  # BCM pin-numbering scheme from Raspberry Pi
    GPIO.setup(lights_pin, GPIO.OUT, initial=GPIO.HIGH)  # Set pin as an output pin with initial state of HIGH

    # GStreamer setup
    Gst.init(None)

    # Start recording session
    print('Waiting %s second(s) before starting' % args.start_time)
    time.sleep(args.start_time)
    print(Fore.GREEN + 'Starting a recording session of %s' % timedelta(seconds=(args.capture_time + args.capture_gap) * args.capture_iterations))

    count=0
    while count < args.capture_iterations:

        if count != 0:
            print('Waiting %s second(s) before starting the next video' % args.capture_gap)
            time.sleep(args.capture_gap)

        print("Switching on the lights")
        GPIO.output(lights_pin, GPIO.HIGH)

        # Get the seconds since epoch
        secondsSinceEpoch = time.time()
        # Convert seconds since epoch to struct_time
        timeObj = time.localtime(secondsSinceEpoch)
        # Get the current timestamp elements from struct_time object i.e.
        print(Fore.GREEN + 'Starting record : %d-%d-%d %d:%d:%d' % (
        timeObj.tm_mday, timeObj.tm_mon, timeObj.tm_year, timeObj.tm_hour, timeObj.tm_min, timeObj.tm_sec))
        # Directory setup
        files_dir=os.path.join(files_location, os.uname().nodename, '%d-%d-%d' % (
        timeObj.tm_year, timeObj.tm_mon, timeObj.tm_mday))
        os.makedirs(files_dir, exist_ok=True)
        # File name setup
        file_name_prefix ='Record_%d-%d-%d_%d-%d-%d' % (
        timeObj.tm_year, timeObj.tm_mon, timeObj.tm_mday, timeObj.tm_hour, timeObj.tm_min, timeObj.tm_sec)
        file_path0 = os.path.join(files_dir, file_name_prefix + '_csi0.mp4')
        file_path1 = os.path.join(files_dir, file_name_prefix + '_csi1.mp4')

        # Build the pipeline
        pipeline_string = ""
        for i, csi in enumerate(args.capture_csi):
            if i > 0:
                pipeline_string += "\n"
            if csi == 0:
                pipeline_string += "nvarguscamerasrc timeout="+ str(args.capture_time) +" sensor_id=0 sensor_mode=4 ! video/x-raw(memory:NVMM), width=1280, height=720, framerate=60/1, format=NV12 ! omxh264enc profile=high ! video/x-h264, level=(string)3.2 ! mp4mux ! filesink location="+ file_path0
            if csi == 1:
                pipeline_string += "nvarguscamerasrc timeout="+ str(args.capture_time) +" sensor_id=1 sensor_mode=3 ! video/x-raw(memory:NVMM), width=1640, height=1232, framerate=30/1, format=NV12 ! nvvidconv flip-method=1 ! video/x-raw(memory:NVMM), format=(string)NV12 ! omxh264enc profile=high ! video/x-h264, level=(string)4 ! mp4mux ! filesink location="+ file_path1

        pipeline = Gst.parse_launch(pipeline_string)
        if not pipeline:
            print("ERROR: Not all elements could be created")
            sys.exit(1)

        # Start capture
        ret = pipeline.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("ERROR: Unable to set the pipeline to the playing state")

        # Wait for EOS or error
        bus = pipeline.get_bus()
        msg = bus.timed_pop_filtered(
            Gst.CLOCK_TIME_NONE,
            Gst.MessageType.ERROR | Gst.MessageType.EOS
        )
        if msg:
            t = msg.type
            if t == Gst.MessageType.ERROR:
                err, dbg = msg.parse_error()
                print("ERROR:", msg.src.get_name(), " ", err.message)
                if dbg:
                    print("debugging info:", dbg)
            elif t == Gst.MessageType.EOS:
                print("End-Of-Stream reached")
            else:
                # this should not happen. we only asked for ERROR and EOS
                print("ERROR: Unexpected message received.")

        # Stop capture
        pipeline.set_state(Gst.State.NULL)

        print("Switching off the lights")
        GPIO.output(lights_pin, GPIO.LOW)

        count += 1

finally:
    if 'pipeline' in locals():
        pipeline.set_state(Gst.State.NULL)
    GPIO.cleanup()
