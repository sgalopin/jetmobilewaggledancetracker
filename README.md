# JetMobileWaggleDanceTracker

Mobile Waggle Dance Tracker with Jetson Nano

## Description
The Jetson was set up with a Jetson nano Jetpack 4.6.1 sd card image (L4T 32.7.1). 
Python scripts were coded to pilot the cameras and the LEDs. 
The main script is using two GStreamer pipelines (chaining of capture, encoding, wrapping and writing) taking advantage 
of the Nvidia hardware acceleration to record simultaneously two types of video in MPEG-4 container format. 
The first pipeline uses the fifth sensor mode (See picture below) of the IMX219 camera sensor and an h264 codec 
(high profile, 3.2 level) to generate the detection videos at a resolution of 720p and a framerate of 60 fps. 
The 120 fps of the sixth sensor mode makes it possible to improve acquisition by capturing at 120 Hz. However, 
the exposure time at this mode will be reduced by half for equal resolution which will alter the precision 
of dance visual analysis. The second pipeline uses the fourth sensor mode and an h264 codec (high profile, 4 levels) 
to generate the hive videos at a resolution of 1640x1232 and a framerate of 30 fps. 

**IMX219 camera sensor modes:**  
![IMX219 camera sensor modes](source/images/Sensor_modes.png)

During recording, the LED states are managed by the script. By default, the duration of the videos was set to 10 minutes 
(to limit the video weight) with the possibility to specify gaps between the records (set to 0 by default). 
Capture times (start and end times) were scheduled with a scheduled task.
Two scripts are used to preview (via an ssh connection) the videos inside the hive without recording to adjust the 
alignment of the camera. A third script gives the possibility to switch the light. The last script wrapped in a 
scheduled task is used to synchronize the videos with a server, one minute after the end of the recording.

## Visuals
**Example of view from the first camera:**    
![IMX219 camera sensor modes](source/images/CSI0_camera_view.png)

**Example of view from the second camera:**    
![IMX219 camera sensor modes](source/images/CSI1_camera_view.png)

## Installation
    sudo apt-get update
    sudo apt-get install python-pip python3-pip
    pip3 install colorama

### Add a cron to automatise the files transfer (on each jetson)
https://doc.ubuntu-fr.org/cron
https://askubuntu.com/questions/142002/generic-solution-to-prevent-a-long-cron-job-from-running-in-parallel/145999#145999
  #### Add the log dir
    cd /home/%JETSON_USERNAME%/jetmobilewaggledancetracker
    mkdir Logs
  #### Add the task
    sudo apt-get install run-one # run-one is a wrapper script that runs no more than one unique instance of some command with a unique set of arguments.
    su jetson
    crontab -e
    */1 9-22 * * * run-one /home/%JETSON_USERNAME%/jetmobilewaggledancetracker/RecordsTransfer.sh >>/home/%JETSON_USERNAME%/jetmobilewaggledancetracker/Logs/cron.log 2>&1
    * 9 * * * run-one /home/%JETSON_USERNAME%/jetmobilewaggledancetracker/CamerasRecords.sh >>/home/%JETSON_USERNAME%/jetmobilewaggledancetracker/Logs/cron.log 2>&1
  #### Uncomment the log line: 
    sudo nano /etc/rsyslog.d/50-default.conf
    uncomment: cron.*                         /var/log/cron.log
  #### Restart: 
    sudo systemctl restart rsyslog
    sudo systemctl restart cron
  #### Check the logs:
    sudo tail -f /var/log/cron.log
  #### Add a ssh public key on the server to avoid password prompt
  https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04-fr

    ssh-keygen # keep passphrase empty
    ssh-copy-id %SERVER_USERNAME%@%SERVER_IP%
  #### Check the logs:
    tail -f /home/%JETSON_USERNAME%/jetmobilewaggledancetracker/Logs/cron.log

## Usage
  #### To manually switch the lights:
    # Parameters
    "-u", "--on": "switch on the lights"
    "-d", "--off": "switch off the lights"

    cd /home/%JETSON_USERNAME%/jetmobilewaggledancetracker
    ./SwitchLight.py --on
    ./SwitchLight.py --off
  #### To get a preview of the first camera: 
    cd /home/%JETSON_USERNAME%/jetmobilewaggledancetracker
    ./Camera0Preview.py
  #### To get a preview of the second camera: 
    cd /home/%JETSON_USERNAME%/jetmobilewaggledancetracker
    ./Camera1Preview.py
  #### To manually launch a recording session:
    # Parameters
    "-ct", "--capture_time": "defines the duration of the videos", type=int, default=60*10 (10min)
    "-cg", "--capture_gap": "defines the gap between the videos", type=int, default=0
    "-ci", "--capture_iterations": "defines the number of videos", type=int, default=6*12 (9h->21h)
    "-cc", "--capture_csi": "defines the cameras to use (0: Inference, 1: Hive)", type=int, default=[0,1] (0:Inference, 1:Hive)
    "-st", "--start_time": "defines a waiting time before starting the sequence of recordings", type=int, default=1

    cd /home/%JETSON_USERNAME%/jetmobilewaggledancetracker
    ./CamerasRecords.py -ct 600 -cg 0 -ci 72 -cc [0,1] -st 1

## Support
Please use the issue tracker.

## Contributing
Open to contributions.

## License
GNU Affero General Public License (AGPL) v3.
