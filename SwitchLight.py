#!/usr/bin/env python3
# -*- coding: utf-8 -*

import RPi.GPIO as GPIO
import sys, getopt

# Pin Definitions
lights_pin = 5  # BCM pin 5, BOARD pin 29


def main(argv):

    lights_pin_state = GPIO.LOW
    default_msg = 'SwitchLight.py -u/-d or --on/-off'

    try:
      opts, args = getopt.getopt(argv,"hud",["on","off"])
    except getopt.GetoptError:
      print(default_msg)
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print(default_msg)
         sys.exit()
      elif opt in ("-u", "--on"):
         lights_pin_state = GPIO.HIGH
      elif opt in ("-d", "--off"):
         lights_pin_state = GPIO.LOW

    # Pin Setup:
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)  # BCM pin-numbering scheme from Raspberry Pi
    GPIO.setup(lights_pin, GPIO.OUT) # Set pin as an output pin with initial state of HIGH
    GPIO.output(lights_pin, lights_pin_state)

    if lights_pin_state == GPIO.HIGH:
        print("Lights switched on !")
    else:
        print("Lights switched off !")
        GPIO.cleanup()


if __name__ == '__main__':
    main(sys.argv[1:])
