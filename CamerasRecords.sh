#!/bin/bash

# Parameters
local_path=/home/%JETSON_USERNAME%/jetmobilewaggledancetracker
capture_time=$((60 * 10)) # default 60*10 (10min)
capture_gap=0 # default 0
capture_iterations=$((6 * 12)) # default 6*12 (9h->21h)
capture_csi='0 1' # default:[0,1] (0:Inference, 1:Hive)
start_time=1

# Launchs the recordings
cd $local_path
./CamerasRecords.py -ct $capture_time -cg $capture_gap -ci $capture_iterations -cc $capture_csi -st $start_time
#./CamerasRecords.py -ci 60
