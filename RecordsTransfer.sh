#!/bin/bash

# Parameters
local_path=/home/%JETSON_USERNAME%/jetmobilewaggledancetracker/Records
remote_path=/media/jetson/video

# Advanced Parameters
remote_user=%SERVER_USERNAME%
remote_ip=%SERVER_IP%

# Moves the records on the analyze computer
cd $local_path
# 2>/dev/null is to avoid error message into log file : "find: ‘*’: No such file or directory"
file_count=$(find * -type f -mmin +1 -printf '.' 2>/dev/null | wc -c)
if [ $file_count -gt 0 ]; then
#  echo "$file_count files found"
  rsync -avzR --remove-source-files -e ssh `find * -type f -mmin +1` $remote_user@$remote_ip:$remote_path/ && find * -type d -empty -delete
#else
#  echo "No files found"
fi
